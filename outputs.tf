# outputs.tf

output "root_zone" {
  value = {
    "arn" : aws_route53_zone.root_zone.arn
    "id" : aws_route53_zone.root_zone.id
    "name" : aws_route53_zone.root_zone.name
    "name_servers" : aws_route53_zone.root_zone.name_servers
    "comment" : aws_route53_zone.root_zone.comment
  }
}

output "record_a_root" {
  value = {
    "enabled" : var.root_a_record.enabled == true ? "true" : "false"
    "name" : var.root_a_record.enabled == true ? aws_route53_record.record_a_root[0].name : null
    "id" : var.root_a_record.enabled == true ? aws_route53_record.record_a_root[0].id : null
    "records" : var.root_a_record.enabled == true ? toset(aws_route53_record.record_a_root[0].records) : null
    "ttl" : var.root_a_record.enabled == true ? aws_route53_record.record_a_root[0].ttl : null
    "type" : var.root_a_record.enabled == true ? aws_route53_record.record_a_root[0].type : null
  }
}

output "records_a" {
  value = toset([
    for key, value in aws_route53_record.record_a : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.records
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "record_alias_root" {
  value = {
    "enabled" : var.root_alias_record.enabled == true ? "true" : "false"
    "healthcheck" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].alias[*].evaluate_target_health : null
    "records" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].alias[*].name : null
    "zone_id" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].alias[*].zone_id : null
    "name" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].fqdn : null
    "id" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].id : null
    "type" : var.root_alias_record.enabled == true ? aws_route53_record.record_alias_root[0].type : null
  }
}

output "records_alias" {
  value = toset([
    for key, value in aws_route53_record.record_alias : {
      (key) : {
        "healthcheck" : value.alias[*].evaluate_target_health
        "records" : value.alias[*].name
        "zone_id" : value.alias[*].zone_id
        "name" : value.fqdn
        "id" : value.id
        "type" : value.type
      }
    }
  ])
}

output "records_cname" {
  value = toset([
    for key, value in aws_route53_record.record_cname : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.records
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "record_mx_root" {
  value = {
    "name" : var.root_mx_records != [] ? aws_route53_record.record_mx_root[0].name : null
    "id" : var.root_mx_records != [] ? aws_route53_record.record_mx_root[0].id : null
    "records" : var.root_mx_records != [] ? toset(aws_route53_record.record_mx_root[0].records) : null
    "ttl" : var.root_mx_records != [] ? aws_route53_record.record_mx_root[0].ttl : null
    "type" : var.root_mx_records != [] ? aws_route53_record.record_mx_root[0].type : null
  }
}

output "record_mx_subdomain" {
  value = toset([
    for key, value in aws_route53_record.record_mx_subdomain : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.records
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "records_txt" {
  value = toset([
    for key, value in aws_route53_record.record_txt : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.records
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "subzone" {
  value = toset([
    for key, value in aws_route53_record.subzone : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.records
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}
