# main.tf

###############################################################################
# DO NOT CHANGE - These resource use for_each to loop through the array that  #
# is defined in terraform.tfvars.json where you can add or change records.    #
###############################################################################

###############################################################################
# Managed Zone Resource                                                       #
###############################################################################

# Zone for root level domain
# Do not change the `name` of this resource since it is used as a data resource
resource "aws_route53_zone" "root_zone" {
  name          = var.dns_zone_fqdn
  comment       = "Root managed DNS zone for ${var.dns_zone_fqdn}"
  force_destroy = true
}

###############################################################################
# A Records                                                                   #
###############################################################################

# Create @ root DNS A record with the IPv4 address value.
resource "aws_route53_record" "record_a_root" {
  count = var.root_a_record.enabled ? 1 : 0

  zone_id = aws_route53_zone.root_zone.zone_id
  name    = aws_route53_zone.root_zone.name
  records = [var.root_a_record.records]
  ttl     = "300"
  type    = "A"

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

# Create @ root DNS Alias record with the ELB value.
resource "aws_route53_record" "record_alias_root" {
  count = var.root_alias_record.enabled ? 1 : 0

  zone_id = aws_route53_zone.root_zone.zone_id
  name    = aws_route53_zone.root_zone.name
  type    = "A"

  alias {
    name                   = var.root_alias_record.records
    zone_id                = var.root_alias_record.zone_id
    evaluate_target_health = var.root_alias_record.healthcheck
  }

  depends_on = [
    aws_route53_zone.root_zone,
  ]

  lifecycle {
    ignore_changes = [
      records,
      ttl
    ]
  }
}

# Create DNS A records for subdomains and hosts with the IPv4 address value.
resource "aws_route53_record" "record_a" {
  for_each = var.a_records

  zone_id = aws_route53_zone.root_zone.zone_id
  name    = "${each.key}.${aws_route53_zone.root_zone.name}"
  records = [each.value]
  ttl     = "300"
  type    = "A"

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

# Create DNS Alias record with the ELB value.
resource "aws_route53_record" "record_alias" {
  for_each = var.alias_records

  zone_id = aws_route53_zone.root_zone.zone_id
  name    = each.value.name
  type    = "A"

  alias {
    name                   = each.value.records
    zone_id                = each.value.zone_id
    evaluate_target_health = each.value.healthcheck
  }

  depends_on = [
    aws_route53_zone.root_zone,
  ]

  lifecycle {
    ignore_changes = [
      records,
      ttl
    ]
  }
}

###############################################################################
# CNAME Records                                                               #
###############################################################################

resource "aws_route53_record" "record_cname" {
  for_each = var.cname_records

  name    = "${each.key}.${aws_route53_zone.root_zone.name}"
  records = [each.value]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.root_zone.zone_id

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

###############################################################################
# MX Records                                                                  #
###############################################################################

# Create @ root DNS MX records
resource "aws_route53_record" "record_mx_root" {
  count = var.root_mx_records != [] ? 1 : 0

  name    = aws_route53_zone.root_zone.name
  records = var.root_mx_records
  ttl     = "300"
  type    = "MX"
  zone_id = aws_route53_zone.root_zone.zone_id

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

# Create subdomain DNS MX records
resource "aws_route53_record" "record_mx_subdomain" {
  for_each = var.subdomain_mx_records

  name    = "${each.key}.${aws_route53_zone.root_zone.name}"
  records = each.value
  ttl     = "300"
  type    = "MX"
  zone_id = aws_route53_zone.root_zone.zone_id

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

###############################################################################
# DNS Subdomain Zones                                                         #
# --------------------------------------------------------------------------- #
# Create DNS record with each of the NS records to add to root level domain   #
# for subdomain zone. The TTL is set at 300 for fast redeployments.           #
###############################################################################

resource "aws_route53_record" "subzone" {
  for_each = var.subzones

  allow_overwrite = true
  name            = "${each.key}.${aws_route53_zone.root_zone.name}"
  records         = each.value
  ttl             = "300"
  type            = "NS"
  zone_id         = aws_route53_zone.root_zone.zone_id

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}

###############################################################################
# TXT records                                                                 #
# --------------------------------------------------------------------------- #
# Be sure to use \" escapes for quotes.                                       #
# Example. "\"v=spf1 include:mailgun.org ~all\""                              #
###############################################################################

# Create DNS Records for TXT Record
resource "aws_route53_record" "record_txt" {
  for_each = var.txt_records

  name    = "${each.key}.${aws_route53_zone.root_zone.name}"
  records = [each.value]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.root_zone.zone_id

  depends_on = [
    aws_route53_zone.root_zone,
  ]
}
