# [terraform-project]/main.tf

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  required_version = ">= 1.0.4"
}

# Providers are defined in providers.tf

# Module with DNS Hosted Zone Configuration
# All DNS records are defined in terraform.tfvars.json
module "aws_route53_hosted_zone" {
  source = "git::https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/terraform-modules/aws/aws-route53-hosted-zone-module.git"

  dns_zone_fqdn        = var.dns_zone_fqdn
  root_a_record        = var.root_a_record
  root_alias_record    = var.root_alias_record
  subzones             = var.subzones
  a_records            = var.a_records
  alias_records        = var.alias_records
  cname_records        = var.cname_records
  root_mx_records      = var.root_mx_records
  subdomain_mx_records = var.subdomain_mx_records
  txt_records          = var.txt_records

}
